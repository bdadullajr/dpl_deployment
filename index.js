const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send({ hello: "world - By: Bernard" });
});

const PORT = process.env.PORT || 8000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});
